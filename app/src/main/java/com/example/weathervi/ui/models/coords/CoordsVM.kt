package com.example.weathervi.ui.models.coords

data class CoordsVM(val latitude: Double?, val longitude: Double?)