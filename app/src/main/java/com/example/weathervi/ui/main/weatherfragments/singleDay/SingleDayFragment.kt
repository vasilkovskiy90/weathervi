package com.example.weathervi.ui.main.weatherfragments.singleDay

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.example.weathervi.R
import com.example.weathervi.databinding.SingleDayFragmentBinding
import com.example.weathervi.domain.models.singledaydatamodel.SingleDayData
import kotlinx.android.synthetic.main.single_day_fragment.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.koin.android.ext.android.inject


class SingleDayFragment : Fragment() {
    private val singleDayFragmentViewModel: SingleDayFragmentViewModel by inject()
    lateinit var binding: SingleDayFragmentBinding


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            LayoutInflater.from(requireContext()),
            R.layout.single_day_fragment,
            container,
            false
        )
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        singleDayFragmentViewModel.getData()
        setObserver()
        addToolbar()
    }

    private fun addToolbar() {
        toolbar.setNavigationIcon(R.drawable.ic_baseline_arrow_back_ios_24)
        toolbar.setNavigationOnClickListener {
            if (activity != null) {
                val intent = activity?.intent
                activity?.finish()
                startActivity(intent)
            }
        }
    }

    private fun setObserver() {
        singleDayFragmentViewModel.singleDayLiveData.observe(viewLifecycleOwner, Observer {
            setDataBinding(it)
        })
    }

    private fun setDataBinding(data: SingleDayData) {
        binding.dataList = data
        binding.executePendingBindings()
    }

}


