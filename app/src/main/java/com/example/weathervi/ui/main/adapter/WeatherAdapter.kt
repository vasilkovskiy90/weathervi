package com.example.weathervi.ui.main.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.example.weathervi.ui.main.weatherfragments.forecast.ForecastFragment
import com.example.weathervi.ui.main.weatherfragments.singleDay.SingleDayFragment


class WeatherAdapter(fa: FragmentActivity): FragmentStateAdapter(fa) {

    override fun createFragment(position: Int): Fragment {
        return when (position) {
            0 -> {
                SingleDayFragment()
            }
            1 -> {
                ForecastFragment()
            }
            else -> {
                SingleDayFragment()
            }
        }
    }

    override fun getItemCount(): Int {
        return 2
    }
}
