package com.example.weathervi.ui.main.weatherfragments.forecast


import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.weathervi.R
import com.example.weathervi.ui.main.weatherfragments.forecast.adapter.ForecastAdapter
import kotlinx.android.synthetic.main.forecast_fragment.*
import org.koin.android.ext.android.inject


class ForecastFragment : Fragment() {
    private val forecastFragmentViewModel: ForecastFragmentViewModel by inject()

    private val forecastAdapter by lazy {
        ForecastAdapter()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.forecast_fragment, container, false)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //drawSome()
        forecastFragmentViewModel.getData()
        setObserver()
        addRecyclerView()
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun setObserver() {
        forecastFragmentViewModel.forecastLiveData.observe(viewLifecycleOwner, Observer {
            forecastAdapter.updateData(it)
        })
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun addRecyclerView() {
        recyclerView.adapter =
            forecastAdapter
        recyclerView.layoutManager =
            LinearLayoutManager(requireContext(), RecyclerView.HORIZONTAL, false)

    }

//    private fun drawSome() {
//        val displayMetrics = DisplayMetrics()
//        requireActivity().windowManager.defaultDisplay.getMetrics(displayMetrics)
//        val width = displayMetrics.widthPixels
//        val height = displayMetrics.heightPixels
//        val bitmap = Bitmap.createBitmap(
//            width,
//            height*2, Bitmap.Config.ARGB_4444
//        )
//        val canvas = Canvas(bitmap)
//
//
//        // canvas background color
//        //canvas.drawARGB(255, 78, 168, 186);
//        resources.displayMetrics.density
//        val paint = Paint()
//        paint.color = Color.parseColor("#FFFFFF")
//        paint.isAntiAlias = true
//        paint.isDither = true
//        val paint2 = Paint()
//        paint2.color = Color.WHITE
//        paint2.strokeWidth = 5F
//        paint2.style = Paint.Style.STROKE
//        paint2.isAntiAlias = true
//        paint2.isDither = true
//        // get device dimensions
//
//        // circle center
//        System.out.println("Width : " + displayMetrics.widthPixels)
//
//        for (index in 1..7) {
//            canvas.drawLine(
//                width - (width / 8) * index.toFloat(), (height / 30).toFloat(),
//                width - (width / 8) * index.toFloat(), height - (height / 30).toFloat(), paint
//            )
//        }
//        canvas.drawLine(
//            (width / 8).toFloat(), height - (height / 30).toFloat(),
//            width - (width / 8).toFloat(), height - (height / 30).toFloat(), paint
//        )
//        canvas.drawCircle((width/8)*1.toFloat(), (height-(height/30)*27).toFloat(), 5f, paint2)
//        canvas.drawCircle((width/8)*2.toFloat(), (height-(height/30)*27).toFloat(), 5f, paint2)
//        canvas.drawCircle((width/8)*3.toFloat(), (height-(height/30)*29).toFloat(), 5f, paint2)
//        canvas.drawCircle((width/8)*4.toFloat(), (height-(height/30)*27).toFloat(), 5f, paint2)
//        canvas.drawCircle((width/8)*5.toFloat(), (height-(height/30)*30).toFloat(), 5f, paint2)
//        canvas.drawCircle((width/8)*6.toFloat(), (height-(height/30)*20).toFloat(), 5f, paint2)
//        canvas.drawCircle((width/8)*7.toFloat(), (height-(height/30)*24).toFloat(), 5f, paint2)
//
//        imageV.background = BitmapDrawable(resources, bitmap)
//    }
}


