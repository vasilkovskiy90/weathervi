package com.example.weathervi.ui.main.weatherfragments.forecast.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.weathervi.R
import com.example.weathervi.databinding.RecyclerViewItemBinding
import com.example.weathervi.domain.models.forecastdatamodel.ForecastDisplay


class ForecastAdapter :
    RecyclerView.Adapter<ForecastAdapter.ForecastViewHolder>() {
    private var dataList = ArrayList<ForecastDisplay>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ForecastViewHolder =
        ForecastViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.recycler_view_item,
                parent,
                false
            )
        )

    override fun onBindViewHolder(holder: ForecastViewHolder, position: Int) {
        holder.bind(dataList[position])
    }

    fun updateData(listForUpdate: ArrayList<ForecastDisplay>) {
        dataList.clear()
        dataList = listForUpdate
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int = dataList.size


    inner class ForecastViewHolder(private var binding: RecyclerViewItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(dataList: ForecastDisplay) {
            binding.dataList = dataList
            binding.executePendingBindings()
        }
    }
}