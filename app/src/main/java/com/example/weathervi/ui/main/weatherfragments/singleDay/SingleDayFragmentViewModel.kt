package com.example.weathervi.ui.main.weatherfragments.singleDay


import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.weathervi.domain.models.singledaydatamodel.SingleDayData
import com.example.weathervi.domain.usecases.ClimaWeatherUseCase
import com.example.weathervi.domain.usecases.ForecastFragmentUseCase
import com.example.weathervi.domain.usecases.OpenWeatherUseCase
import com.example.weathervi.ui.models.coords.CoordsVM
import com.example.weathervi.utils.Constants.CLIMA_WEATHER_SOURCE
import com.example.weathervi.utils.Constants.DATABASE_UPDATE_TIME
import com.example.weathervi.utils.Constants.OPEN_WEATHER_SOURCE
import com.example.weathervi.utils.Constants.TIMER_NAME
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.util.*
import kotlin.concurrent.schedule

class SingleDayFragmentViewModel(
    private val climaWeatherUseCase: ClimaWeatherUseCase,
    private val openWeatherUseCase: OpenWeatherUseCase,
    private val forecastFragmentUseCase: ForecastFragmentUseCase
) : ViewModel() {

    private val _singleDayLiveData = MutableLiveData<SingleDayData>()
    val singleDayLiveData: LiveData<SingleDayData>
        get() = _singleDayLiveData

    fun getData() {
        updateDataTimer()
    }

    private fun updateDataTimer(){
        Timer(TIMER_NAME, false).schedule (0, DATABASE_UPDATE_TIME) {
            GlobalScope.launch {
                getUpdates()
            }
        }
    }
    private suspend fun getUpdates(){
        val singleDayData = forecastFragmentUseCase.getSingleDayForecast()
        val updateTime = Calendar.getInstance().timeInMillis - DATABASE_UPDATE_TIME
        if (singleDayData.time != null) {
            if (updateTime > singleDayData.time) {
                updateData(
                    singleDayData.latitude,
                    singleDayData.longitude,
                    singleDayData.source
                )
                _singleDayLiveData.postValue(forecastFragmentUseCase.getSingleDayForecast())
            } else {
                _singleDayLiveData.postValue(singleDayData)
            }
        }
    }

    private suspend fun updateData(latitude: Double?, longitude: Double?, source: Int?) {
        if (source == OPEN_WEATHER_SOURCE) {
            openWeatherUseCase.getOpenWeatherResponse(CoordsVM(latitude, longitude))
        }
        if (source == CLIMA_WEATHER_SOURCE) {
            climaWeatherUseCase.getClimaWeatherResponse(CoordsVM(latitude, longitude))
        }
    }
}