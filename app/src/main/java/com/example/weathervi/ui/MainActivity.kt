package com.example.weathervi.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.weathervi.R
import com.example.weathervi.ui.main.mainfragment.MainFragment

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                    .replace(R.id.container, MainFragment())
                    .commit()
        }
    }
}