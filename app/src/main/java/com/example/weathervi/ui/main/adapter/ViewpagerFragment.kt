package com.example.weathervi.ui.main.adapter

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import com.example.weathervi.R
import kotlinx.android.synthetic.main.viewpager_fragment.*

class ViewpagerFragment : Fragment(R.layout.viewpager_fragment) {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewpager.adapter = WeatherAdapter(requireActivity())
        viewpager.currentItem = 0

    }
}