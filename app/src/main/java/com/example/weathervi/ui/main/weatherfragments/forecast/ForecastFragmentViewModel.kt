package com.example.weathervi.ui.main.weatherfragments.forecast

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.weathervi.domain.models.forecastdatamodel.ForecastDisplay
import com.example.weathervi.domain.usecases.ForecastFragmentUseCase
import org.koin.core.KoinComponent

class ForecastFragmentViewModel(private val forecastFragmentUseCase: ForecastFragmentUseCase): ViewModel() {

    private val _forecastLiveData = MutableLiveData<ArrayList<ForecastDisplay>>()
    val forecastLiveData: LiveData<ArrayList<ForecastDisplay>>
        get() = _forecastLiveData


    fun getData(){
        _forecastLiveData.value = forecastFragmentUseCase.getForecastData()
    }
}