package com.example.weathervi.ui.main.mainfragment

sealed class DataState {
    object Loading : DataState()
    object End : DataState()
}