package com.example.weathervi.ui.main.mainfragment

import androidx.lifecycle.ViewModel
import com.example.weathervi.domain.usecases.ClimaWeatherUseCase
import com.example.weathervi.domain.usecases.OpenWeatherUseCase
import com.example.weathervi.ui.models.coords.CoordsVM
import com.example.weathervi.utils.Constants
import com.example.weathervi.utils.SingleLiveEvent
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class MainFragmentViewModel(
    private val climaWeatherUseCase: ClimaWeatherUseCase,
    private val openWeatherUseCase: OpenWeatherUseCase
) : ViewModel(){

    var latitude: Double = Constants.LOCATION_NULL_COORDINATE
    var longitude: Double = Constants.LOCATION_NULL_COORDINATE
    val loadingState = SingleLiveEvent<DataState>()

    var serverLiveData = SingleLiveEvent<Int>()

    fun updateLocation(latitude: Double?, longitude: Double?) {
        if (latitude != null && longitude != null) {
            this.latitude = latitude
            this.longitude = longitude
        }
    }

    fun getOpenWeatherResponse() {
        GlobalScope.launch{
            loadingState.postValue(DataState.Loading)
            serverLiveData.postValue(openWeatherUseCase.getOpenWeatherResponse(CoordsVM(latitude, longitude)))
            loadingState.postValue(DataState.End)
        }
    }

   fun getClimaWeatherResponse() {
        GlobalScope.launch {
            loadingState.postValue(DataState.Loading)
            serverLiveData.postValue(climaWeatherUseCase.getClimaWeatherResponse(CoordsVM(latitude, longitude)))
            loadingState.postValue(DataState.End)
        }
   }
}

