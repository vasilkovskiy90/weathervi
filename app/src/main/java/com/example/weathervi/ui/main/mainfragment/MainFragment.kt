package com.example.weathervi.ui.main.mainfragment


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.example.weathervi.R
import com.example.weathervi.ui.main.adapter.ViewpagerFragment
import com.example.weathervi.utils.Constants
import com.example.weathervi.utils.exentions.isNetworkAvailable
import com.google.android.gms.common.api.Status
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.widget.AutocompleteSupportFragment
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.main_fragment.*
import org.koin.android.ext.android.inject


class MainFragment : Fragment() {

    private val fragmentViewModel: MainFragmentViewModel by inject()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.main_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setProgressBar()
        setResponseObserver()
        addAutocompleteApi()
        setButtonListener()
    }

    private fun setProgressBar() {
        fragmentViewModel.loadingState.observe(viewLifecycleOwner, Observer {
            when (it) {
                is DataState.Loading -> progressBar.visibility = View.VISIBLE
                is DataState.End -> progressBar.visibility = View.INVISIBLE
            }
        })
    }

    private fun setResponseObserver() {
        fragmentViewModel.serverLiveData.observe(viewLifecycleOwner, Observer {
            when (it) {
                Constants.RESPONSE_SUCCESS -> {
                    startForecastFragment()
                }
                Constants.RESPONSE_FAIL -> {
                    val snackBarOnResponse = Snackbar.make(
                        requireView(),
                        context?.getText(R.string.on_response_fail).toString(),
                        Snackbar.LENGTH_LONG
                    )
                    val tv =
                        snackBarOnResponse.view.findViewById(com.google.android.material.R.id.snackbar_text) as TextView
                    tv.textAlignment = View.TEXT_ALIGNMENT_CENTER
                    snackBarOnResponse.show()
                }
            }
        })
    }

    private fun startForecastFragment() {
        activity?.supportFragmentManager?.beginTransaction()
            ?.replace(
                R.id.container,
                ViewpagerFragment()
            )
            ?.commit()
    }

    private fun addAutocompleteApi() {
        Places.initialize(
            requireContext(),
            requireContext().getText(R.string.google_api_id) as String
        )
        val autocompleteFragment =
            (childFragmentManager.findFragmentById(R.id.autocomplete_fragment)
                    as AutocompleteSupportFragment?)
        autocompleteFragment?.setPlaceFields(
            listOf(
                Place.Field.ID,
                Place.Field.NAME,
                Place.Field.LAT_LNG
            )
        )
        autocompleteFragment?.setOnPlaceSelectedListener(object : PlaceSelectionListener {
            override fun onPlaceSelected(place: Place) {
                fragmentViewModel.updateLocation(place.latLng?.latitude, place.latLng?.longitude)
                weatherButton.visibility = View.VISIBLE
            }

            override fun onError(status: Status) {
                Snackbar.make(requireView(), status.toString(), Snackbar.LENGTH_LONG).show()
            }
        })
    }

    private fun setButtonListener() {
        weatherButton.setOnClickListener {
            checkConnection()
        }
    }

    private fun checkConnection() {
        if (isNetworkAvailable(requireContext())) {
            weatherButton.isClickable = false
            getResponse()
        } else {
            val snackBarNoInternet = Snackbar.make(
                requireView(),
                requireContext().getText(R.string.connection_error),
                Snackbar.LENGTH_LONG
            )
            val tv =
                snackBarNoInternet.view.findViewById(com.google.android.material.R.id.snackbar_text) as TextView
            tv.textAlignment = View.TEXT_ALIGNMENT_CENTER
            snackBarNoInternet.show()
        }
    }

    private fun getResponse() {
        if (toggleButton.isChecked) fragmentViewModel.getOpenWeatherResponse()
        else fragmentViewModel.getClimaWeatherResponse()
    }
}


