package com.example.weathervi.utils.exentions

import com.facebook.stetho.okhttp3.StethoInterceptor
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

fun retrofitBuilder(baseURL: String): Retrofit.Builder {
    val logging: HttpLoggingInterceptor? = HttpLoggingInterceptor().apply {
        level = HttpLoggingInterceptor.Level.BODY
    }
    val httpClient = OkHttpClient.Builder().apply {
        addNetworkInterceptor(StethoInterceptor())
        if (logging != null) {
            addInterceptor(logging)
        }
    }
    return Retrofit.Builder()
        .baseUrl(baseURL)
        .addCallAdapterFactory(CoroutineCallAdapterFactory())
        .addConverterFactory(GsonConverterFactory.create())
        .client(httpClient.build())
}
