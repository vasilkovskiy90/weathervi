package com.example.weathervi.utils

object Constants {
    const val WEATHER_LOG = "weather_log"
    const val DATABASE_NAME = "Weather_DB"
    const val FORECAST_EIGHT_ELEMENT = 8
    const val UTILS_DIV = 100
    const val LOCATION_NULL_COORDINATE = 0.0
    const val CLIMA_WEATHER_SOURCE = 2
    const val OPEN_WEATHER_SOURCE = 1
    const val API_EXCLUDE_STRING = "hourly,minutely"
    const val API_UNIT_STRING = "metric"
    const val OPEN_WEATHER_API_KEY = "c9c69a580dd4faaae09bd67cc78e8f7b"
    const val CLIMA_WEATHER_API_KEY = "VfVOosbGC9aFNAgz6keKHld5Byk12Iq4"
    const val API_FIELDS_STRING = "temp,sunrise,sunset,weather_code"
    const val API_UNIT_SYSTEM_STRIN = "si"
    const val API_START_TIME_STRING = "now"
    const val OPEN_WEATHER_API_CALL = "onecall"
    const val OPEN_WEATHER_BASE_URL = "https://api.openweathermap.org/data/2.5/"
    const val CLIMA_WEATHER_BASE_URL = "https://api.climacell.co/v3/weather/forecast/"
    const val CLIMA_WEATHER_API_CALL = "daily"
    const val CLIMA_SIMPLE_DATE_FORMAT_PATTERN = "HHmmss"
    const val OPEN_SIMPLE_DATE_FORMAT_PATTERN = "MM.dd"
    const val DATABASE_UPDATE_TIME = 900000L
    const val TIMER_NAME = "UpdateTimer"

    const val CLIMA_MAPPER_MAX_RANGE = 17
    const val CLIMA_MAPPER_MIN_RANGE = 11
    const val CLIMA_MAPPER_SUBSTRING_START_INDEX = 5
    const val CLIMA_MAPPER_OFFSET = 360
    const val API_WEATHER_CODE_THUNDER = 2
    const val API_WEATHER_CODE_DRIZZLE = 3
    const val API_WEATHER_CODE_RAIN = 5
    const val API_WEATHER_CODE_SNOW = 6
    const val API_WEATHER_CODE_FOG = 7
    const val API_WEATHER_CODE_CLOUDY = 8
    const val API_WEATHER_CODE_CLEARDAY = 9
    const val API_WEATHER_CODE_CLEARNIGHT = 10


    const val CLIMA_MAPPER_WEATHER_CODE_THUNDER = 200
    const val CLIMA_MAPPER_WEATHER_CODE_DRIZZLE = 300
    const val CLIMA_MAPPER_WEATHER_CODE_RAIN = 500
    const val CLIMA_MAPPER_WEATHER_CODE_SNOW = 600
    const val CLIMA_MAPPER_WEATHER_CODE_FOG = 700
    const val CLIMA_MAPPER_WEATHER_CODE_CLOUDY = 800
    const val CLIMA_MAPPER_WEATHER_CODE_CLEARDAY = 900
    const val CLIMA_MAPPER_WEATHER_CODE_CLEARNIGHT = 1000

    const val OPEN_WEATHER_MAPPER_DEC = 1000
    const val OPEN_WEATHER_MAPPER_WEATHER_CODE_800 = 800
    const val OPEN_WEATHER_MAPPER_WEATHER_CODE_900 = 900
    const val OPEN_WEATHER_MAPPER_WEATHER_CODE_1000 = 1000

    const val RESPONSE_SUCCESS = 1
    const val RESPONSE_FAIL = 0
    const val NULL_STRING = "null"

    const val FIRST_N = 1
}