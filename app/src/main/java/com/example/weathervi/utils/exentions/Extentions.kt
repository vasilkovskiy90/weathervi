package com.example.weathervi.utils.exentions

import com.example.weathervi.utils.Constants
import kotlin.math.roundToInt

fun String.getDate(): String? {
    return this.replace("-", ".").substring(Constants.CLIMA_MAPPER_SUBSTRING_START_INDEX)
}

fun Double.getTemp(): String {
    return this.roundToInt().toString() + "\u00B0"
}

fun String.getTime(): Int {
    return this.replace(":", "")
        .substring(Constants.CLIMA_MAPPER_MIN_RANGE, Constants.CLIMA_MAPPER_MAX_RANGE)
        .toInt()
}

