package com.example.weathervi.utils


import android.location.Address
import android.location.Geocoder
import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.example.weathervi.R
import com.example.weathervi.utils.Constants.API_WEATHER_CODE_CLEARDAY
import com.example.weathervi.utils.Constants.API_WEATHER_CODE_CLEARNIGHT
import com.example.weathervi.utils.Constants.API_WEATHER_CODE_CLOUDY
import com.example.weathervi.utils.Constants.API_WEATHER_CODE_DRIZZLE
import com.example.weathervi.utils.Constants.API_WEATHER_CODE_FOG
import com.example.weathervi.utils.Constants.API_WEATHER_CODE_RAIN
import com.example.weathervi.utils.Constants.API_WEATHER_CODE_SNOW
import com.example.weathervi.utils.Constants.API_WEATHER_CODE_THUNDER
import com.example.weathervi.utils.Constants.NULL_STRING
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.util.*


object BindingAdapter {
    @BindingAdapter("setWeatherIcon")
    @JvmStatic
    fun setWeatherIcon(textView: TextView, weatherId: Int) {
        when (weatherId.div(Constants.UTILS_DIV)) {
            API_WEATHER_CODE_THUNDER -> textView.text = textView.context.getString(R.string.weather_thunder)
            API_WEATHER_CODE_DRIZZLE -> textView.text = textView.context.getString(R.string.weather_drizzle)
            API_WEATHER_CODE_RAIN -> textView.text = textView.context.getString(R.string.weather_rainy)
            API_WEATHER_CODE_SNOW -> textView.text = textView.context.getString(R.string.weather_snowy)
            API_WEATHER_CODE_FOG -> textView.text = textView.context.getString(R.string.weather_foggy)
            API_WEATHER_CODE_CLOUDY -> textView.text = textView.context.getString(R.string.weather_cloudy)
            API_WEATHER_CODE_CLEARDAY -> textView.text = textView.context.getString(R.string.weather_sunny)
            API_WEATHER_CODE_CLEARNIGHT -> textView.text = textView.context.getString(R.string.weather_clear_night)
            else -> textView.text = NULL_STRING
        }
    }

    @BindingAdapter("setWeatherDescription")
    @JvmStatic
    fun setWeatherDescription(textView: TextView, weatherId: Int) {
        when (weatherId.div(Constants.UTILS_DIV)) {
            API_WEATHER_CODE_THUNDER -> textView.text = textView.context.getString(R.string.w_description_thunder)
            API_WEATHER_CODE_DRIZZLE -> textView.text = textView.context.getString(R.string.w_description_drizzle)
            API_WEATHER_CODE_RAIN -> textView.text = textView.context.getString(R.string.w_description_rainy)
            API_WEATHER_CODE_SNOW -> textView.text = textView.context.getString(R.string.w_description_snowy)
            API_WEATHER_CODE_FOG -> textView.text = textView.context.getString(R.string.w_description_foggy)
            API_WEATHER_CODE_CLOUDY -> textView.text = textView.context.getString(R.string.w_description_cloudy)
            API_WEATHER_CODE_CLEARDAY, API_WEATHER_CODE_CLEARNIGHT -> textView.text = textView.context.getString(R.string.w_description_clear)
            else -> textView.text = NULL_STRING
        }
    }

    @BindingAdapter(value = ["latitude", "longitude"], requireAll = true)
    @JvmStatic
    fun setCityName(textView: TextView, latitude: Double, longitude:Double) {
        GlobalScope.launch(Dispatchers.Main) {
            val geocoder = Geocoder(textView.context, Locale.getDefault())
            val addresses: List<Address> =
                geocoder.getFromLocation(latitude, longitude, 1)
            if (addresses.isEmpty()){
              return@launch
            }
            getGelocationName(addresses[0], textView)
        }
    }

    private fun getGelocationName(address: Address, textView: TextView) {
        when {
            address.locality == null && address.subAdminArea == null && address.adminArea == null -> {
                textView.text = address.countryName
            }
            address.locality == null && address.subAdminArea == null -> {
                textView.text = address.adminArea
            }
            address.locality == null -> {
                textView.text = address.subAdminArea
            }
            else -> {
                textView.text = address.locality
            }
        }
    }
}