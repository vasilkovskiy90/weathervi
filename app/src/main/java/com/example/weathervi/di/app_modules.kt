package com.example.weathervi.di

val app_module = listOf(
    viewModel,
    useCases,
    weatherRepository,
    dataBase
)