package com.example.weathervi.di


import com.example.weathervi.domain.mappers.responsemappers.ClimaMapper
import com.example.weathervi.domain.mappers.responsemappers.OpenWeatherMapper
import com.example.weathervi.domain.usecases.ClimaWeatherUseCase
import com.example.weathervi.domain.usecases.ForecastFragmentUseCase
import com.example.weathervi.domain.usecases.OpenWeatherUseCase
import org.koin.core.module.Module
import org.koin.dsl.module


val useCases: Module = module {
    single {
        ClimaWeatherUseCase(
            get(), ClimaMapper()
        )
    }
    single {
        OpenWeatherUseCase(
            get(), OpenWeatherMapper()
        )
    }
    single{
        ForecastFragmentUseCase(get())
    }

}