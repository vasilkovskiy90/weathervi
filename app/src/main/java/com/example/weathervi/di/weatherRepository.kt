package com.example.weathervi.di


import com.example.weathervi.data.repositories.MainRepositoryImpl
import com.example.weathervi.data.repositories.network.ClimaWeatherRepository
import com.example.weathervi.data.repositories.MainRepository
import com.example.weathervi.data.repositories.fragmentsrepositories.ForecastFragmentRepository
import com.example.weathervi.data.repositories.network.OpenWeatherRepository
import org.koin.core.module.Module
import org.koin.dsl.module


val weatherRepository: Module = module {
    single<MainRepository> { MainRepositoryImpl(
        ClimaWeatherRepository(), OpenWeatherRepository(), get()
    )}
    single {MainRepositoryImpl(
        ClimaWeatherRepository(), OpenWeatherRepository(), get()
    )}
    single { ForecastFragmentRepository(get()) }
}