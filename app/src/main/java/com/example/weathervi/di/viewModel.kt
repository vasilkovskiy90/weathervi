package com.example.weathervi.di


import com.example.weathervi.ui.main.mainfragment.MainFragmentViewModel
import com.example.weathervi.ui.main.weatherfragments.forecast.ForecastFragmentViewModel
import com.example.weathervi.ui.main.weatherfragments.singleDay.SingleDayFragmentViewModel
import org.koin.core.module.Module
import org.koin.dsl.module

val viewModel: Module = module {
    single {
        MainFragmentViewModel(
            get(),
            get()
        )
    }
    single {
        SingleDayFragmentViewModel(
            get(),
           get(),
            get()
        )
    }
    single {
        ForecastFragmentViewModel(
            get()
        )
    }


}