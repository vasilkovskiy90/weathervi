package com.example.weathervi.di

import com.example.weathervi.data.db.WeatherDataBase
import org.koin.android.ext.koin.androidContext
import org.koin.core.module.Module
import org.koin.dsl.module

val dataBase: Module = module {
    single { WeatherDataBase.getInstance(androidContext()) }
}