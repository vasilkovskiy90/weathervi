package com.example.weathervi

import android.app.Application
import com.example.weathervi.di.app_module
import com.facebook.stetho.Stetho
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class WeatherApp : Application() {
    override fun onCreate() {
        super.onCreate()
        Stetho.initializeWithDefaults(this)
        startKoin {
            androidContext(this@WeatherApp)
            modules(app_module)
        }
    }
}