package com.example.weathervi.data.repositories.network.api.climaapi


import com.example.weathervi.utils.Constants
import com.example.weathervi.utils.exentions.retrofitBuilder

object ClimaCellAPI {
    fun makeService(): ClimaApi {
        return retrofitBuilder(Constants.CLIMA_WEATHER_BASE_URL).build().create(ClimaApi::class.java)
    }
}