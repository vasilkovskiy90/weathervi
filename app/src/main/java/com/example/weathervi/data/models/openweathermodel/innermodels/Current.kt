package com.example.weathervi.data.models.openweathermodel.innermodels

data class Current(
    val dt: Int,
    val sunrise: Int,
    val sunset: Int,
    val temp: Double,
    val weather: List<Weather>
)