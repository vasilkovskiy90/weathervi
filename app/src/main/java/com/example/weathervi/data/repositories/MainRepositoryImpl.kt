package com.example.weathervi.data.repositories

import com.example.weathervi.data.repositories.network.ClimaWeatherRepository
import com.example.weathervi.data.repositories.network.OpenWeatherRepository
import com.example.weathervi.data.db.WeatherDataBase
import com.example.weathervi.data.db.entities.WeatherEntity
import com.example.weathervi.data.models.climaforecastmodel.ClimaForecastApiModel
import com.example.weathervi.data.models.openweathermodel.OpenWeatherApiModel

import com.example.weathervi.domain.models.coords.CoordsDomain



class MainRepositoryImpl(
    private val climaWeatherRepository: ClimaWeatherRepository,
    private val openWeatherRepository: OpenWeatherRepository,
    private val db: WeatherDataBase

) : MainRepository {

    override suspend fun getClimaWeatherResponse(coords: CoordsDomain): ClimaForecastApiModel? {
        return climaWeatherRepository.getResponse(coords)
    }

    override suspend fun getOpenWeatherResponse(coords: CoordsDomain): OpenWeatherApiModel? {
        return openWeatherRepository.getResponse(coords)
    }

    override fun updateDB(response: ArrayList<WeatherEntity>) {
        db.clearAllTables()
        db.weatherDAO().insert(response)
    }

    fun getData():ArrayList<WeatherEntity>{
        return  db.weatherDAO().getDataList() as ArrayList<WeatherEntity>
    }
}