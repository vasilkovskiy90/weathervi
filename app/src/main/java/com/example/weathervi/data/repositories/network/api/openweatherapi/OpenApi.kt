package com.example.weathervi.data.repositories.network.api.openweatherapi

import com.example.weathervi.data.models.openweathermodel.OpenWeatherApiModel
import com.example.weathervi.utils.Constants
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface OpenApi {
    @GET(Constants.OPEN_WEATHER_API_CALL)
    suspend fun getForecastAsync(
        @Query("lat") latitude: Double?,
        @Query("lon") longitude: Double?,
        @Query("exclude") exclude: String?,
        @Query("units") units: String?,
        @Query("appid") appid: String?
    ): Response<OpenWeatherApiModel>?
}