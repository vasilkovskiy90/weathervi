package com.example.weathervi.data.repositories.network

import com.example.weathervi.data.models.climaforecastmodel.ClimaForecastApiModel
import com.example.weathervi.domain.models.coords.CoordsDomain

interface ClimaRepositoryInterface {
   suspend fun getResponse(coordsDomain: CoordsDomain): ClimaForecastApiModel?
}