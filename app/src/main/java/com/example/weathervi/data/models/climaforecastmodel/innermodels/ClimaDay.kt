package com.example.weathervi.data.models.climaforecastmodel.innermodels

data class ClimaDay(
    var id: Long = 0,
    val lat: Double,
    val lon: Double,
    val observation_time: ObservationTime?,
    val sunrise: Sunrise?,
    val sunset: Sunset?,
    val temp: List<Temp>?,
    val weather_code: WeatherCode?
)