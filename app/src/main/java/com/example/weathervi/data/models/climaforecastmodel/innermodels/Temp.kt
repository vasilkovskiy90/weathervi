package com.example.weathervi.data.models.climaforecastmodel.innermodels



data class Temp(
    val max: Max,
    val min: Min,
    val observation_time: String = ""
)