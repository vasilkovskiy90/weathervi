package com.example.weathervi.data.models.openweathermodel.innermodels

data class Temp(
    val day: Double
)