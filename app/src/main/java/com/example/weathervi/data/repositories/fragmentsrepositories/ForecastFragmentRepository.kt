package com.example.weathervi.data.repositories.fragmentsrepositories

import com.example.weathervi.data.db.WeatherDataBase
import com.example.weathervi.data.db.entities.WeatherEntity
import org.koin.core.KoinComponent
import org.koin.core.inject

class ForecastFragmentRepository(private val db: WeatherDataBase): KoinComponent {

    fun getForecastData(): List<WeatherEntity> {
        return db.weatherDAO().getDataList()
    }
}



