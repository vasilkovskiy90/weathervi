package com.example.weathervi.data.models.climaforecastmodel.innermodels

data class WeatherCode(
    val value: String = ""
)