package com.example.weathervi.data.repositories

import com.example.weathervi.data.db.entities.WeatherEntity
import com.example.weathervi.data.models.climaforecastmodel.ClimaForecastApiModel
import com.example.weathervi.data.models.openweathermodel.OpenWeatherApiModel
import com.example.weathervi.domain.models.coords.CoordsDomain

interface MainRepository {

    suspend fun getClimaWeatherResponse(coords: CoordsDomain): ClimaForecastApiModel?
    suspend fun getOpenWeatherResponse(coords: CoordsDomain): OpenWeatherApiModel?

    fun updateDB(response: ArrayList<WeatherEntity>)
}