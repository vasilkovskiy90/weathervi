package com.example.weathervi.data.repositories.network

import com.example.weathervi.data.models.openweathermodel.OpenWeatherApiModel
import com.example.weathervi.domain.models.coords.CoordsDomain

interface OpenRepositoryInterface {

   suspend fun getResponse(coordsDomain: CoordsDomain): OpenWeatherApiModel?

}