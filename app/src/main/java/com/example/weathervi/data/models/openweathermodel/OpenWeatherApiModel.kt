package com.example.weathervi.data.models.openweathermodel

import com.example.weathervi.data.models.openweathermodel.innermodels.Current
import com.example.weathervi.data.models.openweathermodel.innermodels.Daily

data class OpenWeatherApiModel(
    val current: Current,
    val daily: List<Daily>,
    val lat: Double,
    val lon: Double
)

