package com.example.weathervi.data.db.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class WeatherEntity(
    @ColumnInfo
    var latitude: Double?,
    var longitude: Double?,
    val temperature: String?,
    val date: String?,
    val weatherCode: Int?,
    var time: Long?,
    val source: Int?
) {
    constructor() : this(0.0, 0.0, "", "", 0, 0, 0)

    @PrimaryKey(autoGenerate = true)
    var id: Int = 0

}