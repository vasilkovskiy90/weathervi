package com.example.weathervi.data.models.climaforecastmodel.innermodels


data class Max(
    val units: String = "",
    val value: Double = 0.0
)