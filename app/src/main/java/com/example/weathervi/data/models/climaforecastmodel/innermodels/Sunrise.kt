package com.example.weathervi.data.models.climaforecastmodel.innermodels

data class Sunrise(
    val value: String = ""
)