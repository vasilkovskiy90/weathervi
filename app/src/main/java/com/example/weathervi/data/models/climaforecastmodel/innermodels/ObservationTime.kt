package com.example.weathervi.data.models.climaforecastmodel.innermodels

data class ObservationTime(
    val value: String = ""
)