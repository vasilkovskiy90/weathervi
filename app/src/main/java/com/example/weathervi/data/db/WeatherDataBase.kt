package com.example.weathervi.data.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.weathervi.data.db.dao.WeatherDao
import com.example.weathervi.data.db.entities.WeatherEntity
import com.example.weathervi.utils.Constants

@Database(
    entities = [
        WeatherEntity::class],
    version = 1
)
abstract class WeatherDataBase : RoomDatabase() {

    abstract fun weatherDAO(): WeatherDao

    companion object {
        @Volatile
        private var INSTANCE: WeatherDataBase? = null
        fun getInstance(context: Context): WeatherDataBase =
            INSTANCE ?: synchronized(this) {
                INSTANCE ?: buildDatabase(context).also { INSTANCE = it }
            }

        private fun buildDatabase(context: Context) =
            Room.databaseBuilder(
                context.applicationContext,
                WeatherDataBase::class.java, Constants.DATABASE_NAME
            )
                .allowMainThreadQueries()
                .build()
    }
}