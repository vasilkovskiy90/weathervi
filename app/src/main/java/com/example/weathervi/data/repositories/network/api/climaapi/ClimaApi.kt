package com.example.weathervi.data.repositories.network.api.climaapi

import com.example.weathervi.data.models.climaforecastmodel.ClimaForecastApiModel
import com.example.weathervi.utils.Constants
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface ClimaApi {
    @GET(Constants.CLIMA_WEATHER_API_CALL)
    suspend fun getForecastAsync(
        @Query("lat") latitude: Double?,
        @Query("lon") longitude: Double?,
        @Query("unit_system") unitSystem: String?,
        @Query("start_time") startTime: String?,
        @Query("fields") fields: String?,
        @Query("apikey") appid: String?
    ): Response<ClimaForecastApiModel>?
}