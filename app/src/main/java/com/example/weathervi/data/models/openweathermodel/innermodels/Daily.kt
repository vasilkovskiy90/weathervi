package com.example.weathervi.data.models.openweathermodel.innermodels

data class Daily(
    val dt: Long,
    val sunrise: Long,
    val sunset: Long,
    val temp: Temp,
    val weather: List<WeatherX>
)