package com.example.weathervi.data.models.climaforecastmodel.innermodels


data class Sunset(
    val value: String = ""
)