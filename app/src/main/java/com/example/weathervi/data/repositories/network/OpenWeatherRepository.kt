package com.example.weathervi.data.repositories.network


import com.example.weathervi.data.models.openweathermodel.OpenWeatherApiModel
import com.example.weathervi.data.repositories.network.api.openweatherapi.OpenWeatherAPI.makeService
import com.example.weathervi.domain.models.coords.CoordsDomain
import com.example.weathervi.utils.Constants

class OpenWeatherRepository: OpenRepositoryInterface {

    override suspend fun getResponse(coordsDomain: CoordsDomain): OpenWeatherApiModel? {
        return try {
            makeService().getForecastAsync(
                coordsDomain.latitude,
                coordsDomain.longitude,
                Constants.API_EXCLUDE_STRING,
                Constants.API_UNIT_STRING,
                Constants.OPEN_WEATHER_API_KEY
            )?.body()
        } catch (t: Throwable){
            t.printStackTrace()
            null
        }
    }
}


