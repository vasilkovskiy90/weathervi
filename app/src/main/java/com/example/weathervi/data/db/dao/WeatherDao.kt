package com.example.weathervi.data.db.dao


import androidx.room.*
import com.example.weathervi.data.db.entities.WeatherEntity


@Dao
interface WeatherDao {

    @Insert
    fun insert(weather: ArrayList<WeatherEntity>):List<Long>

    @Query("SELECT * from WeatherEntity")
    fun getDataList(): List<WeatherEntity>

}