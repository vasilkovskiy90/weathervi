package com.example.weathervi.data.repositories.network

import com.example.weathervi.data.models.climaforecastmodel.ClimaForecastApiModel
import com.example.weathervi.data.repositories.network.api.climaapi.ClimaCellAPI.makeService
import com.example.weathervi.domain.models.coords.CoordsDomain
import com.example.weathervi.utils.Constants

class ClimaWeatherRepository : ClimaRepositoryInterface {

    override suspend fun getResponse(coordsDomain: CoordsDomain): ClimaForecastApiModel? {
        return try {
        makeService().getForecastAsync(
                coordsDomain.latitude,
                coordsDomain.longitude,
                Constants.API_UNIT_SYSTEM_STRIN,
                Constants.API_START_TIME_STRING,
                Constants.API_FIELDS_STRING,
                Constants.CLIMA_WEATHER_API_KEY
            )?.body()
        } catch (t: Throwable){
            t.printStackTrace()
            null
        }
    }
}