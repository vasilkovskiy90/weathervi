package com.example.weathervi.data.repositories.network.api.openweatherapi

import com.example.weathervi.utils.Constants
import com.example.weathervi.utils.exentions.retrofitBuilder

object OpenWeatherAPI {
    fun makeService(): OpenApi {
        return retrofitBuilder(Constants.OPEN_WEATHER_BASE_URL).build().create(OpenApi::class.java)
    }
}