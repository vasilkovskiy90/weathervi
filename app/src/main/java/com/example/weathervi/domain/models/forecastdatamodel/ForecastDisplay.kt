package com.example.weathervi.domain.models.forecastdatamodel

data class ForecastDisplay(
    val latitude: Double?,
    val longitude: Double?,
    val temperature: String?,
    val date: String?,
    val weatherCode: Int?
)