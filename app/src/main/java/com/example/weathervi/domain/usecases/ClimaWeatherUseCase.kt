package com.example.weathervi.domain.usecases

import com.example.weathervi.data.db.entities.WeatherEntity
import com.example.weathervi.data.models.climaforecastmodel.ClimaForecastApiModel
import com.example.weathervi.data.repositories.MainRepositoryImpl
import com.example.weathervi.data.repositories.MainRepository
import com.example.weathervi.domain.mappers.responsemappers.ClimaMapper
import com.example.weathervi.domain.mappers.coordsmapper.CoordsDomainMapper
import com.example.weathervi.ui.models.coords.CoordsVM
import com.example.weathervi.utils.Constants
import com.example.weathervi.utils.Constants.RESPONSE_FAIL
import com.example.weathervi.utils.Constants.RESPONSE_SUCCESS
import java.util.*
import kotlin.collections.ArrayList

class ClimaWeatherUseCase(
    private val mainRepository: MainRepository,
    private val climaMapper: ClimaMapper
) {
    suspend fun getClimaWeatherResponse(coordsVM: CoordsVM): Int {
        val coordsDomain = CoordsDomainMapper().map(coordsVM)
        return when (val response = mainRepository.getClimaWeatherResponse(coordsDomain)) {
            null -> nullData()
            else -> fillDataBase(response)
        }
    }

    private fun fillDataBase(response: ClimaForecastApiModel?): Int {
        mainRepository.updateDB(responseConvert(response))
        return RESPONSE_SUCCESS
    }

    private fun responseConvert(response: ClimaForecastApiModel?): ArrayList<WeatherEntity> {
        val dataList: ArrayList<WeatherEntity> = ArrayList()
        val time = Calendar.getInstance().timeInMillis
        for (day in response?.subList(0, Constants.FORECAST_EIGHT_ELEMENT)!!) {
            dataList.add(climaMapper.map(day).apply {
                this.time = time
            })
        }
        return dataList
    }

    private fun nullData(): Int {
        return RESPONSE_FAIL
    }
}





