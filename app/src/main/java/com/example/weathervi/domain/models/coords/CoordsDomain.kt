package com.example.weathervi.domain.models.coords


data class CoordsDomain(val latitude: Double?, val longitude: Double?)