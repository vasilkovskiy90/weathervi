package com.example.weathervi.domain.models.singledaydatamodel


data class SingleDayData (
    val latitude: Double?,
    val longitude: Double?,
    val temperature: String?,
    val date: String?,
    val weatherCode: Int?,
    val time:Long?,
    val source: Int?
)