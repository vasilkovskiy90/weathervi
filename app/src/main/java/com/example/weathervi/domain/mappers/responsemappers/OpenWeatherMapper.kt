package com.example.weathervi.domain.mappers.responsemappers

import android.annotation.SuppressLint
import com.example.weathervi.data.db.entities.WeatherEntity
import com.example.weathervi.data.models.openweathermodel.innermodels.Daily
import com.example.weathervi.domain.mappers.Mapper
import com.example.weathervi.utils.Constants
import com.example.weathervi.utils.exentions.getTemp

import java.text.SimpleDateFormat
import java.util.*

class OpenWeatherMapper : Mapper<Daily, WeatherEntity> {

    override fun map(input: Daily): WeatherEntity {
        return WeatherEntity(
            latitude = 0.0,
            longitude = 0.0,
            temperature = input.temp.day.getTemp(),
            date = getDate(input.dt),
            weatherCode = getCurrentId(input.weather.first().id, input.sunset, input.sunrise, input.dt),
            time = 0,
            source = Constants.OPEN_WEATHER_SOURCE
        )
    }

    @SuppressLint("SimpleDateFormat")
    private fun getDate(currentDT: Long?):String{
        if (currentDT != null) {
            return  SimpleDateFormat(Constants.OPEN_SIMPLE_DATE_FORMAT_PATTERN).format(Date((currentDT * Constants.OPEN_WEATHER_MAPPER_DEC)))
        }
        return ""
    }

    private fun getCurrentId(
        id: Int?,
        sunset: Long?,
        sunrise: Long?,
        dt: Long?
    ): Int? {

        if (dt == null || sunset == null || sunrise == null) return Constants.OPEN_WEATHER_MAPPER_WEATHER_CODE_1000

        val isClearSky = id == Constants.OPEN_WEATHER_MAPPER_WEATHER_CODE_800

        if (isClearSky && (dt > sunrise && dt < sunset)) {
            return Constants.OPEN_WEATHER_MAPPER_WEATHER_CODE_900
        }
        return id
    }
}