package com.example.weathervi.domain.mappers.responsemappers

import com.example.weathervi.data.db.entities.WeatherEntity
import com.example.weathervi.domain.mappers.sealedconverter.WeatherConverter.Companion.getWeather
import com.example.weathervi.data.models.climaforecastmodel.innermodels.ClimaDay
import com.example.weathervi.domain.mappers.Mapper
import com.example.weathervi.domain.mappers.sealedconverter.WeatherConverter
import com.example.weathervi.utils.Constants
import com.example.weathervi.utils.exentions.getDate
import com.example.weathervi.utils.exentions.getTemp

import kotlin.math.roundToInt


class ClimaMapper :
    Mapper<ClimaDay, WeatherEntity> {
    override fun map(input: ClimaDay): WeatherEntity {
        return WeatherEntity(
            latitude = input.lat,
            longitude = input.lon,
            temperature = input.temp?.last()?.max?.value?.getTemp(),
            date = input.observation_time?.value?.getDate(),
            weatherCode = weatherCodeDescriptor(
                input.weather_code?.value.toString(),
                input.sunset?.value.toString(),
                input.sunrise?.value.toString()
            ),
            time = 0,
            source = Constants.CLIMA_WEATHER_SOURCE
        )
    }


    private fun weatherCodeDescriptor(
        stringValue: String,
        sunset: String,
        sunrise: String
    ): Int? {
        return when (getWeather(stringValue)) {
            is WeatherConverter.Thunder -> WeatherConverter.Thunder().value
            is WeatherConverter.Drizzle -> WeatherConverter.Drizzle().value
            is WeatherConverter.Rain -> WeatherConverter.Rain().value
            is WeatherConverter.Snow -> WeatherConverter.Snow().value
            is WeatherConverter.Fog -> WeatherConverter.Fog().value
            is WeatherConverter.Cloudy -> WeatherConverter.Cloudy().value
            is WeatherConverter.Clear -> WeatherConverter.Clear.getClear(sunrise, sunset)
            is WeatherConverter.NoData -> WeatherConverter.NoData().value
        }
    }
}