package com.example.weathervi.domain.mappers.coordsmapper

import com.example.weathervi.domain.models.coords.CoordsDomain
import com.example.weathervi.domain.mappers.Mapper
import com.example.weathervi.ui.models.coords.CoordsVM

class CoordsDomainMapper() : Mapper<CoordsVM, CoordsDomain> {
    override fun map(input: CoordsVM): CoordsDomain {
        return CoordsDomain(latitude = input.latitude, longitude = input.longitude)
    }
}