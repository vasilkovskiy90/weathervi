package com.example.weathervi.domain.mappers

interface Mapper<I, O> {
    fun map(input: I): O
}