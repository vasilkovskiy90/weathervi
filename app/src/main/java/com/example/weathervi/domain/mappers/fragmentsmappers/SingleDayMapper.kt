package com.example.weathervi.domain.mappers.fragmentsmappers


import com.example.weathervi.data.db.entities.WeatherEntity
import com.example.weathervi.domain.mappers.Mapper
import com.example.weathervi.domain.models.singledaydatamodel.SingleDayData

class SingleDayMapper : Mapper<WeatherEntity, SingleDayData> {
    override fun map(input: WeatherEntity): SingleDayData {
        return SingleDayData(
            latitude = input.latitude,
            longitude = input.longitude,
            temperature = input.temperature,
            date = input.date,
            weatherCode = input.weatherCode,
            time = input.time,
            source = input.source
        )
    }
}