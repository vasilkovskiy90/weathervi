package com.example.weathervi.domain.mappers.fragmentsmappers

import com.example.weathervi.data.db.entities.WeatherEntity
import com.example.weathervi.domain.models.forecastdatamodel.ForecastDisplay
import com.example.weathervi.domain.mappers.Mapper

class ForecastMapper : Mapper<WeatherEntity, ForecastDisplay> {
    override fun map(input: WeatherEntity): ForecastDisplay {
        return ForecastDisplay(
            latitude = input.latitude,
            longitude = input.longitude,
            temperature = input.temperature,
            date = input.date,
            weatherCode = input.weatherCode
        )
    }
}