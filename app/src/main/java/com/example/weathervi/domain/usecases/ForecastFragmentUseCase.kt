package com.example.weathervi.domain.usecases

import com.example.weathervi.data.db.entities.WeatherEntity
import com.example.weathervi.data.repositories.fragmentsrepositories.ForecastFragmentRepository
import com.example.weathervi.domain.mappers.fragmentsmappers.ForecastMapper
import com.example.weathervi.domain.mappers.fragmentsmappers.SingleDayMapper
import com.example.weathervi.domain.models.forecastdatamodel.ForecastDisplay
import com.example.weathervi.domain.models.singledaydatamodel.SingleDayData
import com.example.weathervi.utils.Constants

class ForecastFragmentUseCase(private val forecastFragmentRepository: ForecastFragmentRepository) {

    fun getSingleDayForecast(): SingleDayData {
        return SingleDayMapper().map(forecastFragmentRepository.getForecastData().first())
    }

    fun getForecastData(): ArrayList<ForecastDisplay>?{
        return convertData(forecastFragmentRepository.getForecastData())
    }

    private fun convertData(fullForecast: List<WeatherEntity>): ArrayList<ForecastDisplay>? {
        val dataList: ArrayList<ForecastDisplay> = ArrayList()
        for (day in fullForecast.subList(Constants.FIRST_N, fullForecast.size)) {
            dataList.add(ForecastMapper().map(day))
        }
        return dataList
    }

}