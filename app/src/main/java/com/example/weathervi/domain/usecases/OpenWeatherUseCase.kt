package com.example.weathervi.domain.usecases

import com.example.weathervi.data.db.entities.WeatherEntity
import com.example.weathervi.data.models.openweathermodel.OpenWeatherApiModel
import com.example.weathervi.data.repositories.MainRepositoryImpl
import com.example.weathervi.domain.mappers.coordsmapper.CoordsDomainMapper
import com.example.weathervi.domain.mappers.responsemappers.OpenWeatherMapper
import com.example.weathervi.ui.models.coords.CoordsVM
import com.example.weathervi.utils.Constants
import java.util.*
import kotlin.collections.ArrayList

class OpenWeatherUseCase(
    private val mainRepository: MainRepositoryImpl,
    private val openWeatherMapper: OpenWeatherMapper
) {

    suspend fun getOpenWeatherResponse(coordsVM: CoordsVM): Int {
        val coordsDomain = CoordsDomainMapper().map(coordsVM)
        return when (val response = mainRepository.getOpenWeatherResponse(coordsDomain)) {
            null -> nullData()
            else -> fillDataBase(response)
        }
    }

    private fun responseConvert(body: OpenWeatherApiModel): ArrayList<WeatherEntity> {
        val dataList: ArrayList<WeatherEntity> = ArrayList()
        val lat = body.lat
        val lon = body.lon
        val time = Calendar.getInstance().timeInMillis
        for(item in body.daily){
            dataList.add(
                openWeatherMapper.map(item).apply {
                    this.longitude = lon
                    this.latitude = lat
                    this.time = time
                })
        }
        return dataList
    }

    private fun fillDataBase(response: OpenWeatherApiModel): Int {
        mainRepository.updateDB(responseConvert(response))
        return Constants.RESPONSE_SUCCESS
    }

    private fun nullData(): Int {
        return Constants.RESPONSE_FAIL
    }

}