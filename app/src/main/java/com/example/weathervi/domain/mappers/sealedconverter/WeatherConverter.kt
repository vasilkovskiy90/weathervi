package com.example.weathervi.domain.mappers.sealedconverter

import com.example.weathervi.utils.Constants
import com.example.weathervi.utils.Constants.CLIMA_MAPPER_OFFSET
import com.example.weathervi.utils.Constants.CLIMA_MAPPER_WEATHER_CODE_CLEARDAY
import com.example.weathervi.utils.Constants.CLIMA_MAPPER_WEATHER_CODE_CLEARNIGHT
import com.example.weathervi.utils.Constants.CLIMA_MAPPER_WEATHER_CODE_CLOUDY
import com.example.weathervi.utils.Constants.CLIMA_MAPPER_WEATHER_CODE_DRIZZLE
import com.example.weathervi.utils.Constants.CLIMA_MAPPER_WEATHER_CODE_FOG
import com.example.weathervi.utils.Constants.CLIMA_MAPPER_WEATHER_CODE_RAIN
import com.example.weathervi.utils.Constants.CLIMA_MAPPER_WEATHER_CODE_SNOW
import com.example.weathervi.utils.Constants.CLIMA_MAPPER_WEATHER_CODE_THUNDER
import com.example.weathervi.utils.exentions.getTime
import java.text.SimpleDateFormat
import java.util.*

sealed class WeatherConverter {

    companion object {
        private const val WEATHER_FLURRIES = "flurries"
        private const val WEATHER_RAIN_HEAVY = "rain_heavy"
        private const val WEATHER_STORM = "storm"

        private const val WEATHER_RAIN_LIGHT = "rain_light"
        private const val WEATHER_FREEZING_DRIZZLE = "freezing_drizzle"
        private const val WEATHER_DRIZZLE = "drizzle"

        private const val WEATHER_RAIN = "rain"
        private const val WEATHER_FREEZING_RAIN_HEAVY = "freezing_rain_heavy"
        private const val WEATHER_FREEZING_RAIN = "freezing_rain"
        private const val WEATHER_FREEZING_RAIN_LIGHT = "freezing_rain_light"

        private const val WEATHER_ICE_PELLETS_HEAVY = "ice_pellets_heavy"
        private const val WEATHER_ICE_PELLETS = "ice_pellets"
        private const val WEATHER_ICE_PELLETS_LIGHT = "ice_pellets_light"
        private const val WEATHER_SNOW_HEAVY = "snow_heavy"
        private const val WEATHER_SNOW = "snow"
        private const val WEATHER_SNOW_LIGHT = "snow_light"

        private const val WEATHER_FOG_LIGHT = "fog_light"
        private const val WEATHER_FOG = "fog"

        private const val WEATHER_CLOUDY = "cloudy"
        private const val WEATHER_MOSTLY_CLOUDY = "mostly_cloudy"
        private const val WEATHER_PARTLY_CLOUDY = "partly_cloudy"

        private const val WEATHER_MOSTLY_CLEAR = "mostly_clear"
        private const val WEATHER_CLEAR = "clear"

        fun getWeather(apiWeatherVal: String): WeatherConverter {
            return when (apiWeatherVal) {
                WEATHER_FLURRIES,
                WEATHER_RAIN_HEAVY,
                WEATHER_STORM -> Thunder()

                WEATHER_RAIN_LIGHT,
                WEATHER_FREEZING_DRIZZLE,
                WEATHER_DRIZZLE -> Drizzle()

                WEATHER_RAIN,
                WEATHER_FREEZING_RAIN_HEAVY,
                WEATHER_FREEZING_RAIN,
                WEATHER_FREEZING_RAIN_LIGHT -> Rain()

                WEATHER_ICE_PELLETS_HEAVY,
                WEATHER_ICE_PELLETS,
                WEATHER_ICE_PELLETS_LIGHT,
                WEATHER_SNOW_HEAVY,
                WEATHER_SNOW,
                WEATHER_SNOW_LIGHT -> Snow()

                WEATHER_FOG_LIGHT,
                WEATHER_FOG -> Fog()

                WEATHER_CLOUDY,
                WEATHER_MOSTLY_CLOUDY,
                WEATHER_PARTLY_CLOUDY -> Cloudy()

                WEATHER_MOSTLY_CLEAR,
                WEATHER_CLEAR -> Clear

                else -> NoData()
            }
        }
    }

    class Thunder : WeatherConverter() {
        val value = CLIMA_MAPPER_WEATHER_CODE_THUNDER
    }

    class Drizzle : WeatherConverter() {
        val value = CLIMA_MAPPER_WEATHER_CODE_DRIZZLE
    }

    class Rain : WeatherConverter() {
        val value = CLIMA_MAPPER_WEATHER_CODE_RAIN
    }

    class Snow : WeatherConverter() {
        val value = CLIMA_MAPPER_WEATHER_CODE_SNOW
    }

    class Fog : WeatherConverter() {
        val value = CLIMA_MAPPER_WEATHER_CODE_FOG
    }

    class Cloudy : WeatherConverter() {
        val value = CLIMA_MAPPER_WEATHER_CODE_CLOUDY
    }

    object Clear : WeatherConverter() {
        fun getClear(sunrise: String, sunset: String): Int {
            val currentTime =
                SimpleDateFormat(Constants.CLIMA_SIMPLE_DATE_FORMAT_PATTERN, Locale.ENGLISH)
                    .format(Date()).toInt()
            val sunriseEdited = sunrise.getTime()
            val sunsetEdited = sunset.getTime()
            val offsetFromUtc =
                (TimeZone.getDefault().getOffset(Date().time) / CLIMA_MAPPER_OFFSET)

            return if (currentTime > sunriseEdited + offsetFromUtc && currentTime < sunsetEdited + offsetFromUtc) {
                CLIMA_MAPPER_WEATHER_CODE_CLEARDAY
            } else {
                CLIMA_MAPPER_WEATHER_CODE_CLEARNIGHT
            }
        }
    }

    class NoData(val value: Int? = null) : WeatherConverter()
}
