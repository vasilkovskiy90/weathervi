package com.example.weathervi.model.openWeatherModel;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0010\u0006\n\u0002\b\b\u0018\u00002\u00020\u0001:\u0002\u0019\u001aB\u001b\u0012\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007R\u0013\u0010\b\u001a\u0004\u0018\u00010\t8F\u00a2\u0006\u0006\u001a\u0004\b\n\u0010\u000bR\u0010\u0010\u0005\u001a\u00020\u00068\u0002X\u0083\u0004\u00a2\u0006\u0002\n\u0000R\u0013\u0010\f\u001a\u0004\u0018\u00010\r8F\u00a2\u0006\u0006\u001a\u0004\b\u000e\u0010\u000fR\u001c\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u00038\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u0011R\u001a\u0010\u0012\u001a\u0004\u0018\u00010\u00138\u0006X\u0087\u0004\u00a2\u0006\n\n\u0002\u0010\u0016\u001a\u0004\b\u0014\u0010\u0015R\u001a\u0010\u0017\u001a\u0004\u0018\u00010\u00138\u0006X\u0087\u0004\u00a2\u0006\n\n\u0002\u0010\u0016\u001a\u0004\b\u0018\u0010\u0015\u00a8\u0006\u001b"}, d2 = {"Lcom/example/weathervi/model/openWeatherModel/OpenWeatherForecast;", "", "items", "", "Lcom/example/weathervi/model/openWeatherModel/OpenWeatherForecast$WeatherDaily;", "currentDay", "Lcom/example/weathervi/model/openWeatherModel/OpenWeatherForecast$OpenWeatherCurrentDay;", "(Ljava/util/List;Lcom/example/weathervi/model/openWeatherModel/OpenWeatherForecast$OpenWeatherCurrentDay;)V", "currentDT", "", "getCurrentDT", "()Ljava/lang/Long;", "currentTemp", "", "getCurrentTemp", "()Ljava/lang/String;", "getItems", "()Ljava/util/List;", "lat", "", "getLat", "()Ljava/lang/Double;", "Ljava/lang/Double;", "lon", "getLon", "OpenWeatherCurrentDay", "WeatherDaily", "app_debug"})
public final class OpenWeatherForecast {
    @org.jetbrains.annotations.Nullable()
    @com.google.gson.annotations.SerializedName(value = "lat")
    private final java.lang.Double lat = null;
    @org.jetbrains.annotations.Nullable()
    @com.google.gson.annotations.SerializedName(value = "lon")
    private final java.lang.Double lon = null;
    @org.jetbrains.annotations.NotNull()
    @com.google.gson.annotations.SerializedName(value = "daily")
    private final java.util.List<com.example.weathervi.model.openWeatherModel.OpenWeatherForecast.WeatherDaily> items = null;
    @com.google.gson.annotations.SerializedName(value = "current")
    private final com.example.weathervi.model.openWeatherModel.OpenWeatherForecast.OpenWeatherCurrentDay currentDay = null;
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Long getCurrentDT() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getCurrentTemp() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Double getLat() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Double getLon() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.example.weathervi.model.openWeatherModel.OpenWeatherForecast.WeatherDaily> getItems() {
        return null;
    }
    
    public OpenWeatherForecast(@org.jetbrains.annotations.NotNull()
    java.util.List<com.example.weathervi.model.openWeatherModel.OpenWeatherForecast.WeatherDaily> items, @org.jetbrains.annotations.NotNull()
    com.example.weathervi.model.openWeatherModel.OpenWeatherForecast.OpenWeatherCurrentDay currentDay) {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0004\n\u0002\u0010\u0006\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002R\u001a\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0006X\u0087\u0004\u00a2\u0006\n\n\u0002\u0010\u0007\u001a\u0004\b\u0005\u0010\u0006R\u001a\u0010\b\u001a\u0004\u0018\u00010\t8\u0006X\u0087\u0004\u00a2\u0006\n\n\u0002\u0010\f\u001a\u0004\b\n\u0010\u000b\u00a8\u0006\r"}, d2 = {"Lcom/example/weathervi/model/openWeatherModel/OpenWeatherForecast$OpenWeatherCurrentDay;", "", "()V", "dt", "", "getDt", "()Ljava/lang/Long;", "Ljava/lang/Long;", "temp", "", "getTemp", "()Ljava/lang/Double;", "Ljava/lang/Double;", "app_debug"})
    public static final class OpenWeatherCurrentDay {
        @org.jetbrains.annotations.Nullable()
        @com.google.gson.annotations.SerializedName(value = "dt")
        private final java.lang.Long dt = null;
        @org.jetbrains.annotations.Nullable()
        @com.google.gson.annotations.SerializedName(value = "temp")
        private final java.lang.Double temp = null;
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Long getDt() {
            return null;
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Double getTemp() {
            return null;
        }
        
        public OpenWeatherCurrentDay() {
            super();
        }
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\t\n\u0002\b\u0004\n\u0002\u0010\b\n\u0002\b\u0007\n\u0002\u0010\u000e\n\u0002\b\u0005\u0018\u00002\u00020\u0001:\u0002\u001d\u001eB\u001b\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005\u00a2\u0006\u0002\u0010\u0007R\u0016\u0010\u0002\u001a\u00020\u00038\u0000X\u0081\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u001c\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u00058\u0000X\u0081\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u001a\u0010\f\u001a\u0004\u0018\u00010\r8\u0006X\u0087\u0004\u00a2\u0006\n\n\u0002\u0010\u0010\u001a\u0004\b\u000e\u0010\u000fR\u0013\u0010\u0011\u001a\u0004\u0018\u00010\u00128F\u00a2\u0006\u0006\u001a\u0004\b\u0013\u0010\u0014R\u001a\u0010\u0015\u001a\u0004\u0018\u00010\r8\u0006X\u0087\u0004\u00a2\u0006\n\n\u0002\u0010\u0010\u001a\u0004\b\u0016\u0010\u000fR\u001a\u0010\u0017\u001a\u0004\u0018\u00010\r8\u0006X\u0087\u0004\u00a2\u0006\n\n\u0002\u0010\u0010\u001a\u0004\b\u0018\u0010\u000fR\u0011\u0010\u0019\u001a\u00020\u001a8F\u00a2\u0006\u0006\u001a\u0004\b\u001b\u0010\u001c\u00a8\u0006\u001f"}, d2 = {"Lcom/example/weathervi/model/openWeatherModel/OpenWeatherForecast$WeatherDaily;", "", "dayTemp", "Lcom/example/weathervi/model/openWeatherModel/OpenWeatherForecast$WeatherDaily$DayTemp;", "description", "", "Lcom/example/weathervi/model/openWeatherModel/OpenWeatherForecast$WeatherDaily$WeatherDescription;", "(Lcom/example/weathervi/model/openWeatherModel/OpenWeatherForecast$WeatherDaily$DayTemp;Ljava/util/List;)V", "getDayTemp$app_debug", "()Lcom/example/weathervi/model/openWeatherModel/OpenWeatherForecast$WeatherDaily$DayTemp;", "getDescription$app_debug", "()Ljava/util/List;", "dt", "", "getDt", "()Ljava/lang/Long;", "Ljava/lang/Long;", "id", "", "getId", "()Ljava/lang/Integer;", "sunrise", "getSunrise", "sunset", "getSunset", "tempDailyWithDegree", "", "getTempDailyWithDegree", "()Ljava/lang/String;", "DayTemp", "WeatherDescription", "app_debug"})
    public static final class WeatherDaily {
        @org.jetbrains.annotations.Nullable()
        @com.google.gson.annotations.SerializedName(value = "dt")
        private final java.lang.Long dt = null;
        @org.jetbrains.annotations.Nullable()
        @com.google.gson.annotations.SerializedName(value = "sunrise")
        private final java.lang.Long sunrise = null;
        @org.jetbrains.annotations.Nullable()
        @com.google.gson.annotations.SerializedName(value = "sunset")
        private final java.lang.Long sunset = null;
        @org.jetbrains.annotations.NotNull()
        @com.google.gson.annotations.SerializedName(value = "temp")
        private final com.example.weathervi.model.openWeatherModel.OpenWeatherForecast.WeatherDaily.DayTemp dayTemp = null;
        @org.jetbrains.annotations.NotNull()
        @com.google.gson.annotations.SerializedName(value = "weather")
        private final java.util.List<com.example.weathervi.model.openWeatherModel.OpenWeatherForecast.WeatherDaily.WeatherDescription> description = null;
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String getTempDailyWithDegree() {
            return null;
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Long getDt() {
            return null;
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Long getSunrise() {
            return null;
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Long getSunset() {
            return null;
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Integer getId() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.example.weathervi.model.openWeatherModel.OpenWeatherForecast.WeatherDaily.DayTemp getDayTemp$app_debug() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.util.List<com.example.weathervi.model.openWeatherModel.OpenWeatherForecast.WeatherDaily.WeatherDescription> getDescription$app_debug() {
            return null;
        }
        
        public WeatherDaily(@org.jetbrains.annotations.NotNull()
        com.example.weathervi.model.openWeatherModel.OpenWeatherForecast.WeatherDaily.DayTemp dayTemp, @org.jetbrains.annotations.NotNull()
        java.util.List<com.example.weathervi.model.openWeatherModel.OpenWeatherForecast.WeatherDaily.WeatherDescription> description) {
            super();
        }
        
        @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0006\n\u0002\b\u0006\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002R\"\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0010\n\u0002\u0010\t\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\b\u00a8\u0006\n"}, d2 = {"Lcom/example/weathervi/model/openWeatherModel/OpenWeatherForecast$WeatherDaily$DayTemp;", "", "()V", "temp", "", "getTemp", "()Ljava/lang/Double;", "setTemp", "(Ljava/lang/Double;)V", "Ljava/lang/Double;", "app_debug"})
        public static final class DayTemp {
            @org.jetbrains.annotations.Nullable()
            @com.google.gson.annotations.SerializedName(value = "day")
            private java.lang.Double temp;
            
            @org.jetbrains.annotations.Nullable()
            public final java.lang.Double getTemp() {
                return null;
            }
            
            public final void setTemp(@org.jetbrains.annotations.Nullable()
            java.lang.Double p0) {
            }
            
            public DayTemp() {
                super();
            }
        }
        
        @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0006\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002R\u001e\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0086\u000e\u00a2\u0006\u0010\n\u0002\u0010\t\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\b\u00a8\u0006\n"}, d2 = {"Lcom/example/weathervi/model/openWeatherModel/OpenWeatherForecast$WeatherDaily$WeatherDescription;", "", "()V", "id", "", "getId", "()Ljava/lang/Integer;", "setId", "(Ljava/lang/Integer;)V", "Ljava/lang/Integer;", "app_debug"})
        public static final class WeatherDescription {
            @org.jetbrains.annotations.Nullable()
            private java.lang.Integer id;
            
            @org.jetbrains.annotations.Nullable()
            public final java.lang.Integer getId() {
                return null;
            }
            
            public final void setId(@org.jetbrains.annotations.Nullable()
            java.lang.Integer p0) {
            }
            
            public WeatherDescription() {
                super();
            }
        }
    }
}