package com.example.weathervi.utils;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0017\u0010\u0005\u001a\u0004\u0018\u00010\u00062\b\u0010\u0007\u001a\u0004\u0018\u00010\b\u00a2\u0006\u0002\u0010\tJ\u0017\u0010\n\u001a\u0004\u0018\u00010\u00062\b\u0010\u0007\u001a\u0004\u0018\u00010\b\u00a2\u0006\u0002\u0010\tR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000b"}, d2 = {"Lcom/example/weathervi/utils/SetWeatherData;", "", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "setWeatherDescription", "", "actualId", "", "(Ljava/lang/Integer;)Ljava/lang/String;", "setWeatherIcon", "app_debug"})
public final class SetWeatherData {
    private final android.content.Context context = null;
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String setWeatherIcon(@org.jetbrains.annotations.Nullable()
    java.lang.Integer actualId) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String setWeatherDescription(@org.jetbrains.annotations.Nullable()
    java.lang.Integer actualId) {
        return null;
    }
    
    public SetWeatherData(@org.jetbrains.annotations.NotNull()
    android.content.Context context) {
        super();
    }
}