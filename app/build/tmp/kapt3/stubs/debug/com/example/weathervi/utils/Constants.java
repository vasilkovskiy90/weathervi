package com.example.weathervi.utils;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\t\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0006\n\u0002\b\u0006\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u000eX\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0016"}, d2 = {"Lcom/example/weathervi/utils/Constants;", "", "()V", "API_EXCLUDE_STRING", "", "API_FIELDS_STRING", "API_START_TIME_STRING", "API_UNIT_STRING", "API_UNIT_SYSTEM_STRIN", "CLIMA_WEATHER_API_CALL", "CLIMA_WEATHER_API_KEY", "CLIMA_WEATHER_BASE_URL", "DATABASE_NAME", "FORECAST_EIGHT_ELEMENT", "", "LOCATION_NULL_COORDINATE", "", "OPEN_WEATHER_API_CALL", "OPEN_WEATHER_API_KEY", "OPEN_WEATHER_BASE_URL", "UTILS_DIV", "WEATHER_LOG", "app_debug"})
public final class Constants {
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String WEATHER_LOG = "WeatherLog";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String DATABASE_NAME = "WeatherVI_DB";
    public static final int FORECAST_EIGHT_ELEMENT = 8;
    public static final int UTILS_DIV = 100;
    public static final double LOCATION_NULL_COORDINATE = 0.0;
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String API_EXCLUDE_STRING = "hourly,minutely";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String API_UNIT_STRING = "metric";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String OPEN_WEATHER_API_KEY = "c9c69a580dd4faaae09bd67cc78e8f7b";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String CLIMA_WEATHER_API_KEY = "VfVOosbGC9aFNAgz6keKHld5Byk12Iq4";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String API_FIELDS_STRING = "temp,sunrise,sunset,weather_code";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String API_UNIT_SYSTEM_STRIN = "si";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String API_START_TIME_STRING = "now";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String OPEN_WEATHER_API_CALL = "onecall";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String OPEN_WEATHER_BASE_URL = "https://api.openweathermap.org/data/2.5/";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String CLIMA_WEATHER_BASE_URL = "https://api.climacell.co/v3/weather/forecast/";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String CLIMA_WEATHER_API_CALL = "daily";
    public static final com.example.weathervi.utils.Constants INSTANCE = null;
    
    private Constants() {
        super();
    }
}